# Pegando a imagem
FROM node:16.16.0

# Metadado indicando quem mantém
LABEL maintainer="vladfarias@kaueandrade.com"

# Seta um comando ou processo que roda a cada vez que um conteiner é executado a partir de uma imagem
CMD [ "cmd", "AC04 entregue!" ]